Area dedicated to machine learning studies for DESY summer 2017 student project

Stuy of the following hyper-parameter optimization techniques:

- GridSearchCV/RandomizedSearchCV: Exhaustive/Randomized parameter searches
- HyperOpt: Sequetial model-based optimization via Bayesian optimization with Gaussian Process or Tree-sturctured of Parzen Estimators (TPE) 
- Bayes_Opt Bayesian optimization constructing a posterior distribution of functions for gaussian process, 
- SkOpt: Bayesian optimization with acquisition function: Expected improvement (EI), Lower Confidence bound (LCB), and Probability Improvement (PI) 
- Optunity: Primarily based on PSO