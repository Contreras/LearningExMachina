# ---- Import from root_numpy library 
import root_numpy
from root_numpy import root2array, rec2array

# ---- Import common python libraries
import sys
import time
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random

# ---- Import from root_pandas library
import root_pandas
from root_pandas import read_root

# ---- Import panda library
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix
from pandas.core.index import Index
import pandas.core.common as com

# ---- Import scipy
import scipy
from scipy.stats import ks_2samp
import scipy as sp

# ----Import itertools
import itertools
from itertools import cycle

# Import Jupyter
from IPython.core.interactiveshell import InteractiveShell

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasClassifier
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.constraints import maxnorm
from keras.models import model_from_json

# ----- Import scikit-learn
import sklearn
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import RFE
from sklearn.feature_selection import SelectFromModel

from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold, StratifiedKFold, ShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve

from sklearn.svm import SVC, LinearSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, VotingClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier, BaggingClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import RandomizedLasso

from sklearn.preprocessing import StandardScaler, RobustScaler, MinMaxScaler
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.feature_selection import RFECV
import sklearn.svm
from sklearn.calibration import calibration_curve, CalibratedClassifierCV
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.metrics import (confusion_matrix, roc_auc_score, roc_curve, 
                             auc, average_precision_score, precision_score, 
                             brier_score_loss, recall_score, f1_score, log_loss, 
                             classification_report, precision_recall_curve, accuracy_score)

from sklearn.dummy import DummyClassifier

from sklearn.externals import joblib
from sklearn import feature_selection

# ---- Import imblearn
import imblearn
from imblearn.over_sampling import ADASYN, SMOTE, RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler


# ----- Python utilities
from collections import defaultdict, Counter

# Python regular-expression
import re

# ---- Sciki-kit learn graph 
from sklearn.tree import export_graphviz

# ---- Check the versions of libraries/packages
print("Python version " + sys.version)
print("Sklearn version " + sklearn.__version__)
print("Root_numpy version " + root_numpy.__version__)
print("Numpy version " + np.__version__)
print("Scipy version " + scipy.__version__)
print("Pandas version " + pd.__version__)
print("Matplotlib version " + matplotlib.__version__)
print("Seaborn version " + sns.__version__)
print("Imblance version " +imblearn.__version__)

# ---- Fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# ---- Data loading function
def load(sig_filename, bkg_filename, category, features):
    """Data loader.
    
    Parameters
    ----------
    sig_filename : array, shape = [n_samples]
            true class, intergers in [0, n_classes - 1)
    bkg_filename : array, shape = [n_samples, n_classes]
    category: string
    features: array, shape = [n_features]

    Returns
    -------
    data : pandas.DataFrame
    """
   
    signal = read_root([sig_filename], category, columns=features)
    background = read_root([bkg_filename], category, columns=features)
    
    signal['y'] = 1.
    background['y'] = 0.
      
    # for sklearn data is usually organised
    # into one 2D array of shape (n_samples x n_features)
    # containing all the data and one array of categories
    # of length n_samples
    data = pd.concat([signal, background])

    return data


# ---- Feature names
branch_names = """mass_tag_tag_min_deltaR,median_mass_jet_jet,
    maxDeltaEta_tag_tag,mass_higgsLikeDijet,HT_tags,
    btagDiscriminatorAverage_tagged,mass_jet_tag_min_deltaR,
    mass_jet_jet_min_deltaR,mass_tag_tag_max_mass,maxDeltaEta_jet_jet,
    centrality_jets_leps,centrality_tags,globalTimesEventWeight""".split(",")

features = [c.strip() for c in branch_names]
features = (b.replace(" ", "_") for b in features)
features = list(b.replace("-", "_") for b in features)

# --- Load dataset
signal_sample = "combined/signalMC.root"
background_sample = "combined/backgroundMC.root"
tree_category = "event_mvaVariables_step7_cate4"

data = load(signal_sample, background_sample, tree_category, features)

print "Total number of events: {}\nNumber of features: {}".format(data.shape[0], data.shape[1])

# ---- Store a copy for later use
data_archived = data.copy(deep=True)


# ---- Create features dataframe and target array
data_X = data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False)
data_y = data["y"]
input_dim = data_X.shape[1]

# ---- Split dataset
X_train, X_test, Y_train, Y_test = train_test_split(data_X, data_y, test_size=0.33, random_state=seed)


# ---- Function to create model, required for KerasClassifier
def create_model(nlayers=1, nneurons=100, dropout_rate=0.0, l2_norm=1e-3, 
                 activation='relu', init_mode='uniform', optimizer='adam'):
    
    # create model
    model = Sequential()
    
    # indicate the number of layers
    for layer in range(nlayers):
        model.add(Dense(nneurons, input_dim=input_dim,
                        init=init_mode,
                        activation=activation, W_regularizer=l2(l2_norm)))
        model.add(Dropout(dropout_rate))
    model.add(Dense(1, init=init_mode, activation='sigmoid', 
                    W_regularizer=l2(l2_norm)))
    
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    
    return model

## HyperOpt provides a nice way to perform automated hyperparameter tuning without the cost associated with a grid search ##

# ---- Hyper-paramter tuning using HyperOpt library
from hyperopt.pyll import scope
from hyperopt import hp
from hyperopt import fmin, Trials, STATUS_OK, tpe, space_eval


# ---- Definite the objective class used to minimize on
class Objective:
    
    # Class constructor initialize data members
    def __init__(self, X, y, n_folds=3):
        self.X = X
        self.y = y
        self.best = 0
        self.seed = 42
        self.n_folds= n_folds
        self.cv = KFold(n_splits=self.n_folds, shuffle=True, random_state=self.seed)

    # Define the objective function
    def __call__(self, params):
        auc_score = self.hyperopt_train_test(params)

        # Involving accuracy
        #if auc_score > self.best:
        #    self.best = acc
        #print 'new best:', self.best, params
        #return {'loss': -auc_score, 'status': STATUS_OK}
        print('SCORE:', auc_score, params)
        return{'loss': 1-auc_score, 'status': STATUS_OK }
    
    # Set the classifier algorithm
    def setEstimator(self, Classifier):
        self.Classifier = Classifier
    
    # Handles the evaluation to be minimized
    def hyperopt_train_test(self, params):
        self.Classifier.set_params(**params)

        scoring = 'roc_auc'#None, 'accuracy'
        score = cross_val_score(estimator=self.Classifier, X=self.X, y=self.y, cv=self.cv, scoring=scoring, n_jobs=-1).mean()
        
        # Note: hyperopt works by minimizing an objective function and sampling parameter values from various distributions. 
        # Since the goal is to maximize the AUC score, we will have hyperopt minimize 1 - AUC.
        return 1 - score

# ---- Configure parameter search space
space = {
    'kerasclassifier__nlayers'     : hp.choice('nlayers', range(1, 3)),
    'kerasclassifier__nneurons'    : hp.choice('nneurons', (150, 300, 600)),
    'kerasclassifier__l2_norm'     : hp.choice('l2_norm', (0.001, 0.01)),
    'kerasclassifier__dropout_rate': hp.choice('dropout_rate', (0.1, 0.2, 0.4))
}

space_sampling = {
    'kerasclassifier__nlayers'     : hp.quniform('nlayers', 1, 3, 1),
    'kerasclassifier__nneurons'    : hp.quniform('nneurons', 150, 250, 50),
    'kerasclassifier__l2_norm'     : hp.uniform('l2_norm', 0.001, 0.01),
    'kerasclassifier__dropout_rate': hp.uniform('dropout_rate', 0.1, 0.4)
}


# ---- Preprocessing using 0-1 scaling byremoving the mean and scaling to unit variance 
scaler = RobustScaler()

# ---- Create model for use in scikit-learn
pipe_classifiers = {
    'deepNN':        make_pipeline(scaler,  KerasClassifier(build_fn=create_model, batch_size=128, nb_epoch=50, verbose=0)),
}

# --- Instantiate the objective function
objective = Objective(X_train.values, Y_train.values)
objective.setEstimator(pipe_classifiers['deepNN'])

# ---- The Trials object will store details of each iteration (keeps track of all experiments)
# These can be saved and loaded back into a new batch of experiments
trials = Trials()

# ---- Number of maximum evalutions to run in experiment
max_evals=10#300

# ---- Run the hyperparameter search using the tpe algorithm (minimize the objective over the space)
process = time.clock()
best = fmin(fn=objective, 
            space=space, 
            algo=tpe.suggest, # Tree-structured Parzen estimator algorithm
            max_evals=max_evals, 
            trials=trials)

print "Optimization time: %.3f"%(time.clock()-process)

# ---- Best contains the encoding used by hyperopt to get the parameter values from the space dictionary
print('HyperOpt encoded values ', best)

# ---- Get the values of the optimal parameters
best_params = space_eval(space, best)
print('Best fit: ',best_params)
print('Max loss: ',max(trials.losses()))
print('Min loss: ',min(trials.losses()))

# ---- Fit the model with the optimal hyperparamters
pipe = pipe_classifiers['deepNN']
pipe.set_params(**best_params)
pipe.fit(X_train, Y_train);

# ---- Score with the test data
y_score = pipe.predict_proba(X_test)
auc_score = roc_auc_score(Y_test, y_score[:,1])
print("Test roc auc: ",auc_score)

# ----- Save the hyperparameter at each iteration to a csv file
param_values = [x['misc']['vals'] for x in trials.trials]
param_values = [{key:value for key in x for value in x[key]} for x in param_values]
param_values = [space_eval(space, x) for x in param_values]

param_df = pd.DataFrame(param_values)
param_df['auc_score'] = [1 - x for x in trials.losses()]
param_df.index.name = 'Iteration'
param_df.to_csv("parameter_values.csv")

# A while later ...

# Load the parameter values
param_df = pd.read_csv('parameter_values.csv')

# Add column for loss
param_df['loss'] = param_df['auc_score'].map(lambda x: 1-x)

# ---- Plot the loss at each iteration
plt.semilogy(np.arange(1,max_evals+1), param_df['loss'], 'ko', markersize=2)
plt.xlabel('Iteration')
plt.ylabel('Loss')
plt.title('Loss at each iteration')
plt.show()
