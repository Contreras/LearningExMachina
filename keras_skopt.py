# ---- Import from root_numpy library 
import root_numpy
from root_numpy import root2array, rec2array

# ---- Import common python libraries
import sys
import time
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random

# ---- Import from root_pandas library
import root_pandas
from root_pandas import read_root

# ---- Import panda library
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix
from pandas.core.index import Index
import pandas.core.common as com

# ---- Import scipy
import scipy
from scipy.stats import ks_2samp
import scipy as sp

# ----Import itertools
import itertools
from itertools import cycle

# Import Jupyter
from IPython.core.interactiveshell import InteractiveShell

# ---- Keras deep neural network library
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.optimizers import SGD
from keras.wrappers.scikit_learn import KerasClassifier
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.constraints import maxnorm
from keras.models import model_from_json

# ----- Import scikit-learn
import sklearn
from sklearn.feature_selection import VarianceThreshold
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import RFE
from sklearn.feature_selection import SelectFromModel

from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold, StratifiedKFold, ShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve

from sklearn.svm import SVC, LinearSVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, VotingClassifier
from sklearn.ensemble import GradientBoostingClassifier, AdaBoostClassifier, BaggingClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import RandomizedLasso

from sklearn.preprocessing import StandardScaler, RobustScaler, MinMaxScaler
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.feature_selection import RFECV
import sklearn.svm
from sklearn.calibration import calibration_curve, CalibratedClassifierCV
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.metrics import (confusion_matrix, roc_auc_score, roc_curve, 
                             auc, average_precision_score, precision_score, 
                             brier_score_loss, recall_score, f1_score, log_loss, 
                             classification_report, precision_recall_curve, accuracy_score)

from sklearn.dummy import DummyClassifier

from sklearn.externals import joblib
from sklearn import feature_selection

# ---- Import imblearn
import imblearn
from imblearn.over_sampling import ADASYN, SMOTE, RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler


# ----- Python utilities
from collections import defaultdict, Counter

# Python regular-expression
import re

# ---- Sciki-kit learn graph 
from sklearn.tree import export_graphviz

# ---- Check the versions of libraries/packages
print("Python version " + sys.version)
print("Sklearn version " + sklearn.__version__)
print("Root_numpy version " + root_numpy.__version__)
print("Numpy version " + np.__version__)
print("Scipy version " + scipy.__version__)
print("Pandas version " + pd.__version__)
print("Matplotlib version " + matplotlib.__version__)
print("Seaborn version " + sns.__version__)
print("Imblance version " +imblearn.__version__)

# ---- Fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# ---- Data loading function
def load(sig_filename, bkg_filename, category, features):
    """Data loader.
    
    Parameters
    ----------
    sig_filename : array, shape = [n_samples]
            true class, intergers in [0, n_classes - 1)
    bkg_filename : array, shape = [n_samples, n_classes]
    category: string
    features: array, shape = [n_features]

    Returns
    -------
    data : pandas.DataFrame
    """
   
    signal = read_root([sig_filename], category, columns=features)
    background = read_root([bkg_filename], category, columns=features)
    
    signal['y'] = 1.
    background['y'] = 0.
      
    # for sklearn data is usually organised
    # into one 2D array of shape (n_samples x n_features)
    # containing all the data and one array of categories
    # of length n_samples
    data = pd.concat([signal, background])

    return data


# ---- Feature names
branch_names = """mass_tag_tag_min_deltaR,median_mass_jet_jet,
    maxDeltaEta_tag_tag,mass_higgsLikeDijet,HT_tags,
    btagDiscriminatorAverage_tagged,mass_jet_tag_min_deltaR,
    mass_jet_jet_min_deltaR,mass_tag_tag_max_mass,maxDeltaEta_jet_jet,
    centrality_jets_leps,centrality_tags,globalTimesEventWeight""".split(",")

features = [c.strip() for c in branch_names]
features = (b.replace(" ", "_") for b in features)
features = list(b.replace("-", "_") for b in features)

# --- Load dataset
signal_sample = "combined/signalMC.root"
background_sample = "combined/backgroundMC.root"
tree_category = "event_mvaVariables_step7_cate4"

data = load(signal_sample, background_sample, tree_category, features)

print "Total number of events: {}\nNumber of features: {}".format(data.shape[0], data.shape[1])

# ---- Store a copy for later use
data_archived = data.copy(deep=True)


# ---- Create features dataframe and target array
data_X = data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False)
data_y = data["y"]
input_dim = data_X.shape[1]

# ---- Split dataset
X_train, X_test, Y_train, Y_test = train_test_split(data_X, data_y, test_size=0.33, random_state=seed)


# ---- Function to create model, required for KerasClassifier
def create_model(nlayers=1, nneurons=100, dropout_rate=0.0, l2_norm=1e-3, 
                 activation='relu', init_mode='uniform', optimizer='adam'):
    
    # create model
    model = Sequential()
    
    # indicate the number of layers
    for layer in range(nlayers):
        model.add(Dense(nneurons, input_dim=input_dim,
                        init=init_mode,
                        activation=activation, W_regularizer=l2(l2_norm)))
        model.add(Dropout(dropout_rate))
    model.add(Dense(1, init=init_mode, activation='sigmoid', 
                    W_regularizer=l2(l2_norm)))
    
    # Compile model
    model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    
    return model

# ---- Scikit-Learn Optimizer
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_evaluations

# ---- Python utilities
from collections import defaultdict, Counter

# ---- Handles objective function
class ObjectiveClass:
    
    # constuctor
    def __init__(self, X, y, n_folds=3, scoring='roc_auc'):
        # Split development set into a train and test set
        self.X = X
        self.y = y
        self.seed = 42
        self.n_folds= n_folds
        self.cv = KFold(n_splits=self.n_folds, shuffle=True, random_state=self.seed)
        self.scoring = scoring
 
    # objective function we want to minimize 
    def __call__(self, values):
        # Build paramter list with updated values
        params_dict = dict(zip(self.keys, values))

        # Optimize hyper-parameters of classifier
        self.clf.set_params(**params_dict)

        # Cross-validation mean absolute error of a classifier as a function of its hyper-parameters
        score = cross_val_score(self.clf, self.X, self.y, cv=self.cv, scoring=self.scoring, n_jobs=-1).mean()

        return -score #1-score
    
    def setEstimator(self, Classifier):
        self.clf = Classifier
        
    def paramKeys(self, keys):
        self.keys = keys

import collections

# ---- Set configuration space
parameters = collections.OrderedDict(
    [
        ('kerasclassifier__nlayers', (1, 3)),
        ('kerasclassifier__nneurons', (150, 300)),
        ('kerasclassifier__l2_norm', (0.01, 0.1)), 
        ('kerasclassifier__dropout_rate', (0.01, 0.1))
    ])

# ---- Preprocessing using 0-1 scaling byremoving the mean and scaling to unit variance 
scaler = RobustScaler()

# ---- Create model for use in scikit-learn
pipe_classifiers = {
    'deepNN':  make_pipeline(scaler,  KerasClassifier(build_fn=create_model, batch_size=128, epochs=50, verbose=0))
    }

# ---- Instantiate objective function
objective = ObjectiveClass(data_X.values, data_y.values)
objective.setEstimator(pipe_classifiers['deepNN'])
objective.paramKeys(parameters.keys())

# ---- Bayesian optimization
process = time.clock()
clf_gp_ei = gp_minimize(func=objective, dimensions=parameters.values(), n_calls=30, random_state=0, acq_func="EI", n_jobs=-1)
print "Optimization time: %.3f"%(time.clock()-process)
print("Best score=%.4f (EI)" % clf_gp_ei.fun)

print("""EI best parameters:
- nlayers= %d
- nneurons= %d
- dropout_rate= %.6f
- l2_norm= .6f""" % (clf_gp_ei.x[0], clf_gp_ei.x[1], 
                     clf_gp_ei.x[2], clf_gp_ei.x[3]))

process = time.clock()
clf_gp_lcb = gp_minimize(func=objective, dimensions=parameters.values(), n_calls=30, random_state=0, acq_func="LCB", n_jobs=-1)
print "Optimization time: %.3f"%(time.clock()-process)
print("Best score=%.4f (LCB)" % clf_gp_lcb.fun)

print("""LCB best parameters:
- nlayers= %d
- nneurons= %.d
- dropout_rate= %.6f
- l2_norm= %.6f""" % (clf_gp_lcb.x[0], clf_gp_lcb.x[1],
                      clf_gp_lcb.x[2], clf_gp_lcb.x[3]))

process = time.clock()
clf_gp_pi  = gp_minimize(func=objective, dimensions=parameters.values(), n_calls=30, random_state=0, acq_func="PI", n_jobs=-1)
print "Optimization time: %.3f"%(time.clock()-process)
print("Best score=%.4f (PI)" % clf_gp_pi.fun)

print("""PI best parameters:
- nlayers= %d
- nneurons= %d
- dropout_rate= %.6f
- l2_norm= %.6f""" % (clf_gp_pi.x[0], clf_gp_pi.x[1],
                      clf_gp_pi.x[2], clf_gp_pi.x[3]))

# ---- Evalution
plot_evaluations(clf_gp_ei, bins=10)
plt.show()

plot_evaluations(clf_gp_lcb, bins=10)
plt.show()

plot_evaluations(clf_gp_pi, bins=10)
plt.show()

# ---- Convergence
plot_convergence(clf_gp_ei);
plt.show()

plot_convergence(clf_gp_lcb);
plt.show()

plot_convergence(clf_gp_pi);
plt.show()

