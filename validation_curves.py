## Script used to determine possible ranges for hyper-parameter values used in optimization

# ---- Import common python libraries
import sys
import time
import math
import heapq
import os.path
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import random
import operator
import collections

# ---- Import from root_numpy library
import root_numpy
from root_numpy import root2array, rec2array

# ---- Import from root_pandas library
import root_pandas
from root_pandas import read_root

# Import panda library
import pandas.core.common as com
from pandas.tools import plotting
from pandas.tools.plotting import scatter_matrix
from pandas.core.index import Index

# ---- Import libraries
import ROOT
from ROOT import TMVA


# ---- Keras deep neural network library
from keras.wrappers.scikit_learn import KerasClassifier
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.regularizers import l1, l2 #,WeightRegularizer
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization

# ---- Scikit-Learn Optimizer
from skopt import gp_minimize, forest_minimize
from skopt.plots import plot_convergence
from skopt.plots import plot_evaluations


# Import scikit-learn
import sklearn

from sklearn.preprocessing import (StandardScaler, RobustScaler, MinMaxScaler,
                                   LabelEncoder, OneHotEncoder)

from sklearn import feature_selection
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.model_selection import ParameterGrid
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold, StratifiedKFold, ShuffleSplit
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve
from sklearn.model_selection import validation_curve


# ---- Select set of variables to use in MVA modeling
def get_variables(set="dl"):
    vars = {
        "dl" : ["pT_jet_tag_min_deltaR", "multiplicity_higgsLikeDijet15", "maxDeltaEta_tag_tag",
              "centrality_tags","mass_tag_tag_max_mass","H3_tag","btagDiscriminatorAverage_tagged",
              "btagDiscriminatorAverage_untagged","maxDeltaEta_jet_jet","mass_tag_tag_min_deltaR"]
    }
    return vars[set]


# ---- Fix random seed for reproducibility
seed = 7
np.random.seed(seed)


# ---- Data loading function
def load(sig_filename, bkg_filename, category, features):
    """Data loader.
    
    Parameters
    ----------
    sig_filename : array, shape = [n_samples]
            true class, intergers in [0, n_classes - 1)
    bkg_filename : array, shape = [n_samples, n_classes]
    category: string
    features: array, shape = [n_features]

    Returns
    -------
    data : pandas.DataFrame
    """
   
    signal = read_root([sig_filename], category, columns=features)
    background = read_root([bkg_filename], category, columns=features)
    
    signal['y'] = 1.
    background['y'] = 0.
      
    # for sklearn data is usually organised
    # into one 2D array of shape (n_samples x n_features)
    # containing all the data and one array of categories
    # of length n_samples
    data = pd.concat([signal, background])

    return data


# ---- Feature names
branch_names = """mass_tag_tag_min_deltaR,median_mass_jet_jet,
    maxDeltaEta_tag_tag,mass_higgsLikeDijet,HT_tags,
    btagDiscriminatorAverage_tagged,mass_jet_tag_min_deltaR,
    mass_jet_jet_min_deltaR,mass_tag_tag_max_mass,maxDeltaEta_jet_jet,
    centrality_jets_leps,centrality_tags,globalTimesEventWeight""".split(",")

features = [c.strip() for c in branch_names]
features = (b.replace(" ", "_") for b in features)
features = list(b.replace("-", "_") for b in features)

# --- Load dataset
signal_sample     = "data/signalMC.root"
background_sample = "data/backgroundMC.root"
tree_category     = "event_mvaVariables_step7_cate4"

data = load(signal_sample, background_sample, tree_category, features)

print "Total number of events: {}\nNumber of features: {}".format(data.shape[0], data.shape[1])

# ---- Create features dataframe and target array
data_X = data.drop(["y","globalTimesEventWeight"], axis=1, inplace=False)
data_y = data["y"]
input_dim = data_X.shape[1]


# ---- Split dataset
X_train, X_test, y_train, y_test = train_test_split(data_X, data_y, test_size=0.33, random_state=seed)

# ---- Set deep neural network architecture
def create_model(nlayers=1, nneurons=300, dropout_rate=0.0,
                 l2_norm=0.001, activation='relu', init_mode='lecun_normal',
                 learning_rate=1e-3, save_as='model.h5', input_dim=12):

    # prevent from giving float value                                                                                                    
    nlayers = int(nlayers)
    nneurons = int(nneurons)

    # create model 
    model = Sequential()

    # indicate the number of layers  
    model.add(Dense(nneurons,
                        input_dim=input_dim,
                        kernel_initializer=init_mode,
                        activation=activation,
                        kernel_regularizer=l2(l2_norm)))
    model.add(Dropout(dropout_rate))

    for layer in range(nlayers-1):
        model.add(Dense(nneurons,
                        kernel_initializer=init_mode,
                        activation=activation,
                        kernel_regularizer=l2(l2_norm)))
        model.add(Dropout(dropout_rate))

    # output layer 
    model.add(Dense(2, kernel_initializer=init_mode, activation='softmax',
                        kernel_regularizer=l2(l2_norm)))


    # compile model (set loss and optimize)
    model.compile(loss='categorical_crossentropy',
                  optimizer=Adam(lr=learning_rate), metrics=['accuracy'])

    # Store model to file
    model.save(save_as)

    # print summary report 
    model.summary()

    return model


## Define validation plots
def plot_validation_curve(models, X_train, y_train, param_name,
                          params, param_range, cv=3,
                          scoring="neg_log_loss", logx=False,
                          n_jobs=1):
    """
    Draw histogram of the DataFrame's series comparing the distribution
    in `signal` to `background`.

    Parameters
    ----------
    models : dictionary, shape = [n_models]
    X : DataFrame, shape = [n_samples, n_classes]
    y : DataFrame, shape = [n_classes]
    param_range :

    param_name :

    cv :
    scoring :
    n_jobs :

    Returns
    -------
    plot : matplotlib plot
    """

    """
    Describe possible kwargs values
    
    Keys
    ----------
    """

    # line width
    lw = 2

    # check to see if models is a list
    if not isinstance(models, list):
        models = [models]

    # check to see if model is a pipeline object or not
    if isinstance(models[0], sklearn.pipeline.Pipeline):
        data_type = type(models[0]._final_estimator)
    else:
        data_type = type(models[0])

    # plot title
    name = filter(str.isalnum, str(data_type).split(".")[-1])
    title = "Validation Curves (%s)" % name

    # create blank canvas
    fig, ax = plt.subplots(figsize=(8, 6))

    ax.grid(which='major', linestyle='-', linewidth='0.2', color='gray')
    #ax.set_facecolor('white')

    for n, model in enumerate(models):
        # validation scores
        train_scores, test_scores = validation_curve(
            model, X_train, y_train,
            param_name=param_name,
            param_range=param_range, cv=cv,
            scoring=scoring, n_jobs=n_jobs)

        # mean train scores
        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std  = np.std(train_scores, axis=1)

        # mean test scores
        test_scores_mean  = np.mean(test_scores, axis=1)
        test_scores_std   = np.std(test_scores, axis=1)

        # extract information for legend
        label = 'placeholder ('
        for i, p_name in enumerate(params.keys()):
            param = model.get_params()[p_name]
            if i != len(params.keys())-1:
                label = label+p_name.replace(name.lower()+'__','')+'=%.1f, ' % param
            else:
                label = label+p_name.replace(name.lower()+'__','')+'=%.1f' % param
        label=label+')'

        # plot validation curve
        if logx is True:
            plt.semilogx(param_range, train_scores_mean, '--',
                         label=label.replace('placeholder','Training'),
                         color="darkorange", lw=lw)

            plt.semilogx(param_range, test_scores_mean,
                         label=label.replace('placeholder','Test'),
                         color="navy", lw=lw)
        else:
            plt.plot(param_range, train_scores_mean, '--',
                     label=label.replace('placeholder','Training'),
                     color="darkorange", lw=lw)

            plt.plot(param_range, test_scores_mean,
                     label=label.replace('placeholder','Test'),
                     color="navy", lw=lw)

        plt.fill_between(param_range, train_scores_mean - train_scores_std,
                         train_scores_mean + train_scores_std, alpha=0.2,
                         color="darkorange", lw=lw)

        plt.fill_between(param_range, test_scores_mean - test_scores_std,
                         test_scores_mean + test_scores_std, alpha=0.2,
                         color="navy", lw=lw)

        #colour = test_line[-1].get_color()
        #plt.axvline(x=best_iter, color=colour)

    # plot title
    plt.title(title, fontsize=14)

    plt.xlabel(str(param_name).replace(name.lower()+'__',''))
    plt.ylabel(scoring)

    # x-axis range
    plt.xlim([min(param_range), max(param_range)])

    plt.legend(loc='best', frameon=False, fancybox=True, fontsize=12)

    return plt.show()


if __name__ == "__main__":

    # ---- Preprocessing using 0-1 scaling by removing the mean and scaling to unit variance 
    scaler = RobustScaler()

    # ---- Create model for use in scikit-learn
    pipe_classifiers = {
        'keras':  make_pipeline(scaler,  KerasClassifier(build_fn=create_model, batch_size=128, epochs=50, verbose=0))
        }

    name = 'KerasClassifier'.lower()
    
    param_name = name+'__nneurons'

    param_range = np.linspace(1, 10, num=10, dtype=int)

    params = {name.lower()+'__nneurons': 2}

    plot_validation_curve(pipe_classifiers['keras'], 
                          X_train, y_train, 
                          param_name,
                          params, 
                          param_range, 
                          cv=3,
                          scoring="roc_auc", 
                          logx=False,
                          n_jobs=1)

