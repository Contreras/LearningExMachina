# ---- Import libraries
import ROOT
from ROOT import TMVA

# ---- Import matplotlib
import matplotlib.pyplot as plt

# ---- Import additional ibraries
import numpy as np
from scipy import interpolate as ip
from array import array

def get_roc_curve(hsig, hbkg):

    # Bet bins
    nbins = hsig.GetNbinsX()
    nsig  = hsig.GetEntries()
    nbkg  = hbkg.GetEntries()

    # Use for true positive rate and fase positive rate values
    tprs = []
    fprs = []

    for ibin in range(1, nbins+1):
        true_negativ = 0
        false_negativ = 0

        for jbin in range(1, ibin+1):
            true_negativ += hsig.GetBinContent(jbin)
            false_negativ += hbkg.GetBinContent(jbin)

        true_positive_rate = (nsig-true_negativ)/nsig
        false_positive_rate = (nbkg-false_negativ)/nbkg

        tprs.append(true_positive_rate)
        fprs.append(1-false_positive_rate)

    #roc_curve  = ROOT.TGraph(nbins, array('f', tprs), array('f',fprs))
    #roc_curve.Draw()

    return tprs, fprs

for VARSET, variable in zip(["dl"], ["BDT"]):

    # Set output file for ROC curve
    hfile = ROOT.TFile("ClassificationOutput_" + VARSET +".root", "READ")
    hsig = hfile.Get("hsig_output")
    hbkg = hfile.Get("hbkg_output")

    x,y = get_roc_curve(hsig, hbkg)
    
    # Plot ROC curve
    plt.plot(x,y, lw=2, alpha=0.8,label=variable+ " (AUC={:.3f})".format(np.abs(np.trapz(y,x))))

# ROC curve plotted with matplotlib
ax = plt.gca()
# ax.tick_params(top="on", right="on")
ax.get_xaxis().set_tick_params(which="both", direction="in")
ax.get_yaxis().set_tick_params(which="both", direction="in")

plt.minorticks_on()
plt.xlim([0,1])
plt.ylim([0,1])
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.xlabel("Signal Efficiency", fontsize=14)
plt.ylabel("Background Rejection", fontsize=14)
plt.grid(True, ls=':', alpha=0.8)
legend=plt.legend(loc="lower left", fontsize=11, frameon=False)

plt.savefig('roc_curve.pdf')

plt.show()
