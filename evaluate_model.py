import sys

# ---- Import libraries
import ROOT
from ROOT import TMVA

import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate as ip

from array import array
# ---- Choose TMVA method to train and evaluate
methodName = "BDT" 

# ---- Provide Input file
path_to_training_trees = "data/"
sigSrcTrain     = ROOT.TFile( path_to_training_trees + "signalTraining.root" )
bkgSrcTrain     = ROOT.TFile( path_to_training_trees + "backgroundTraining.root" )
sigSrcTest      = ROOT.TFile( path_to_training_trees + "signalTesting.root" )
bkgSrcTest      = ROOT.TFile( path_to_training_trees + "backgroundTesting.root" )

tree = "event_mvaVariables_step7_cate3"
sigTreeTrain = sigSrcTrain.Get( tree )
bkgTreeTrain = bkgSrcTrain.Get( tree )
sigTreeTest  = sigSrcTest.Get(  tree )
bkgTreeTest  = bkgSrcTest.Get(  tree )


def get_variables(set="4j4b"):
    vars = {
        "dl" : ["pT_jet_tag_min_deltaR", "multiplicity_higgsLikeDijet15", "maxDeltaEta_tag_tag",
                "centrality_tags","mass_tag_tag_max_mass","H3_tag","btagDiscriminatorAverage_tagged",
                "btagDiscriminatorAverage_untagged","maxDeltaEta_jet_jet","mass_tag_tag_min_deltaR"]
        }
    return vars[set]

def fill_hist(name, tree, hist, VARSET):

    reader = TMVA.Reader("V:Silent")
    var_array = []

    # Include variables
    for var in get_variables(VARSET):
        var_array.append(array('f', [0]))
        tree.SetBranchAddress(var, var_array[-1])
        reader.AddVariable(var, var_array[-1])

    print hist.GetName()
    methodName = "BDT_"+ VARSET
    #weight_file = "ttH_dataset/weights/TMVAClassification_BDT" + VARSET + ".weights.xml"
    weight_file = "ttH_dataset/weights/TMVAClassification_BDT.weights.xml"

    # Boook method
    reader.BookMVA(methodName, weight_file)

    # Sample weight
    sample_weight = 1.

    # Fill histogram
    for ievt in range(tree.GetEntries()):
        tree.GetEntry(ievt)
        hist.Fill(reader.EvaluateMVA(methodName), sample_weight)


for VARSET in ["dl"]:
    # Ouput file
    outfile = ROOT.TFile("ClassificationOutput_" + VARSET +".root", "RECREATE")
    print("ClassificationOutput_" + VARSET +".root")

    # Set the number of bins to use
    nbins = 100 #1000 

    # Set signal histogram
    hsig = ROOT.TH1F("hsig_output", "BDT Output Signal", nbins, -1, 1)
    hsig.SetLineColor(ROOT.kBlue)

    # Set background histogram
    hbkg = ROOT.TH1F("hbkg_output", "BDT Output Background", nbins, -1, 1)
    hbkg.SetLineColor(ROOT.kRed)

    # Fill signald and background histograms
    fill_hist("Signal", sigTreeTest, hsig, VARSET)
    fill_hist("Background", bkgTreeTest, hbkg, VARSET)
    
    print VARSET

    hsig.Write()
    hbkg.Write()
    outfile.Close()
